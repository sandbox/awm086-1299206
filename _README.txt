
Cutom Title Widget
------------------------
by Ali Almustafa - awm086@gmail.com


Description
-----------
This is an underdevelopemt light module that allows to change the widget of the title field of any content type in drupal7.
It currently supports changing the title widget from a regular text field to a select list, and/or auto complete field where the user defines the allowed values.
The plan is to develop this module to allow the use of more custom widgets and enable users to use custom php to generate allowed values when it is needed.

Note that there may be other ways to change the widget for the title. However, I was not able to find a way and hence the  module.

Installation
------------
Extract the module in you sites/all/modules directory




Usage
-------------
1. Enable the module
2. When editing a content type the module adds Title Widget Configuration.
	For example, to change the title widget of the native content type 'Article' go to (/admin/structure/types/manage/article)
3. Choose from the currently available widget and make sure to add the allowed values for Select List and/or autocomplete in the allowed values text area.
4. Save content type.




