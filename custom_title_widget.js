(function ($) {

Drupal.behaviors.custom_title_widgetFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-custom-title-widget', context).drupalSetSummary(function (context) {

      // Retrieve the value of the selected radio button
      var ctw = $("input[@name=#edit-custom-title-widget-ctw]:checked").val();

      if (ctw==0) {
        return Drupal.t('Disabled')
      }
      else if (ctw==1) {
        return Drupal.t('Select List')
      }
      else if (ctw==2) {
        return Drupal.t('Autocomplete')
      }
    });
  }
};

})(jQuery);
